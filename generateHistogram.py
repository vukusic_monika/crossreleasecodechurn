import csv #knjiznica potrebana za ucitavanje csv file-ova
import numpy as np #matematicka knjiznica za izracun parametara za histogram
import matplotlib.pyplot as plt #matematicka knjiznica za iscrtavanje grafova
from mpl_toolkits.mplot3d import Axes3D #knjiznica potrebna za 3D projekciju histograma, omogucuje plotanje 3D objekata na 2D przoru

headerNotExist = True 

readerCurrentFile = open("writeOldestResults.csv", 'r') 
readerCurrentFileDict = csv.DictReader(readerCurrentFile, delimiter=",", quoting=csv.QUOTE_NONE)
headers = readerCurrentFileDict.fieldnames 

relativeDifferenceArray = [] 
bugCntArray = []
metrics = [] 
counter = 0
while(headerNotExist):
	metric = raw_input("Please enter metric for correlation count: ") 
	if metric in headers:
		headerNotExist = False

for index, row in enumerate(readerCurrentFileDict):
	if ( float(row['bug_cnt']) != 0.0 and float(row['Absolute difference ' + metric]) != 0.0  and float(row['Relative difference ' + metric]) != 0.0 ):
		bugCntArray.append(float(row['bug_cnt']))
		metrics.append(float(row[metric]))
		relativeDifferenceArray.append(float(row['Relative difference ' + metric]))
	else:
		counter+=1  

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#DEFAULT BIN JE 10, OVISNO O NJEMU RADIMO MATRICU
#print relativeDifferenceArray
hist, xedges, yedges = np.histogram2d(bugCntArray,relativeDifferenceArray, bins=8, range=[[0,10],[0,15]]) 

#AKO ZADAMO BIN 8, DOBIJEMO RUBOVE BIN+1
#ZATO U MESHGRIDU UVIJEK ODUZMEMO ZADNJI CLAN xedges[:-1]
xpos, ypos = np.meshgrid(xedges[:-1], yedges[:-1])

xpos = xpos.flatten('F')
ypos = ypos.flatten('F')
zpos = np.zeros_like(xpos)
deltaX = np.ones(len(zpos))
deltaY = np.ones(len(zpos))
deltaZ = hist.flatten('F')
"""
print len(xpos)
print len(ypos)
print len(zpos)
print len(deltaZ)
print deltaZ
print counter
"""
plt.title(metric)
plt.xlabel("Rel. diff " + metric)
plt.ylabel("Bug count")
ax.bar3d(xpos, ypos, zpos, deltaX, deltaY, deltaZ, color='red')
plt.show()