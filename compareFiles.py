import os
import sys
import csv
import glob
import pandas
import re

#unos putanje i trenutne verzije, prazan input se ne prihvaca
flag = 1;
while flag:
	path = raw_input("Please enter path: ")

	if not path:
		print "Please enter path again!"
	else:
		flag = 0

#dohvat putanje direktorija koji sadrzi sve verzije programskog koda
fmask = os.path.join(path, '*.csv')
csvFiles = glob.glob(fmask)

#ispis svih csv datoteka uz indekse
i = 0
files = []
print "Available versions:"
for index, file in sorted(enumerate(csvFiles), key=lambda file: file[1]):
	print str(i) + " - " + file #dohvati sve csv fileove iz zadanog direktorija
	files.append(file)
	i = i+1

#od korisnika se trazi unos indeksa trenutne verzije
currentFileIndex = raw_input("Please enter current version index: ")
#provjerava se je li unesen indeks validan
if currentFileIndex.isdigit() and int(currentFileIndex) < len(csvFiles):
	currentFileIndex = int(currentFileIndex)
	currentFile = files[currentFileIndex]

#ispis svih csv datoteka uz indekse
print "Available versions for comparing:"
for index in range(currentFileIndex):
	print str(index) + " - " + csvFiles[index] #dohvati sve csv fileove iz zadanog direktorija

#od korisnika se trazi unos indeksa verzija s kojima se zeli usporedjivati sve dok unos nije validan
flag = 1
#varijabla koja ce pratiti jesu li sve unesene verzije ispravne
flagV = 0
while flag:
	version = raw_input("\nEnter comma seperated index(es) of file(s): ")
	inputVersions = version.split(",")
	flagV = 0
	for v in inputVersions:
		#ako nadjemo verziju koja nije validna povecavamo brojac flagV
		if not(v.isdigit() and int(v) < currentFileIndex and int(v) < len(csvFiles) and int(v) >= 0):
			flagV = flagV + 1
	#ako je flagV brojac ostao 0 sve unesene verzije su validne i prekidamo izvodjenje while petlje
	if flagV == 0:
		flag = 0
	#u suprotnom od korisnika trazimo ponovni unos
	else:
		print "Unavailable version selected. Please enter again."

#za sve indekse koje je korisnik unio odvojene zarezima se dodaju u polje selectedVersion
versions = version.split(",")
selectedVersion = []
for v in versions:
	if v.strip().isdigit():
		selectedVersion.append(int(v.strip()))

#otvaranje trenutne verzije datoteke te zapisivanje rijecnika u readerCurrentFileDict varijablu
readerCurrentFile = open(currentFile, 'r')
readerCurrentFileDict = csv.DictReader(readerCurrentFile, delimiter=",", quoting=csv.QUOTE_NONE)

#izrada polja headers i ucitavanje imena klase te svih metrika trenutne verzije
headers = []
headersMetric = readerCurrentFileDict.fieldnames
headersLenMetric = len(headersMetric)

#import data by columns
data = pandas.read_csv(currentFile)
bug_cnt_SUM = 0
LOCsum = 0
#make sum for LOC and bug_cnt column
#print data.LOC
LOCsum = sum(data.LOC)
#print LOCsum
bug_cnt_SUM = sum(data.bug_cnt)

#u zaglavlje se dodaje stupac File
headers.append(readerCurrentFileDict.fieldnames[0])
for field in readerCurrentFileDict.fieldnames:
	#preskace se dodavanje File stupca jer je vec unesen
	if field == "File":
		continue
	#dodavanje metrike
	headers.append(field)
	for v in selectedVersion:
		#dodavanje apsolutne i relativne razlike za sve metrike u header
		headers.append('Absolute difference ' + field + " - V" + str(v))
		headers.append('Relative difference ' + field + " - V" + str(v))

#za svaku verziju koju je korisnik odabrao dodaje se stupac ClassFound
for v in selectedVersion:
	headers.append('ClassFound - V' + str(v))

#dodaje se stupac za defect density
headers.append('DD')
#dodaj stupac za izracun prosjecnog broja pogresaka
headers.append('bug_cnt_avg')
#dodaj stupac za izracun prosjecnog broja linija koda
headers.append('LOC_avg')
#dodaj stupac za binarni klasifikator
headers.append('bug_cnt_bin')

#otvaranje svih verzija datoteka s kojima se zeli usporedjivati
readerCompareListFiles = []
readerCompareList = []
previousFile = []
for v in selectedVersion:
	previousFile = re.split(r'(\W+)', csvFiles[v]) 
	#otvaranje datoteke i pohrana u rijecnik
	readerFileCurrentVersion = open(csvFiles[v], 'r')
	readerFileCurrentVersionDict = csv.DictReader(readerFileCurrentVersion, delimiter=",", quoting=csv.QUOTE_NONE)
	#spremanje svih verzija u listu
	readerCompareList.append(readerFileCurrentVersionDict)
	readerCompareListFiles.append(readerFileCurrentVersion)

#otvaranje datoteke za zapisivanje rezultata mjerenja te zapisivanje headera
print csvFiles[0]

writeFileName = re.split(r'(\W+)', currentFile)
writeFileName =  "writeResults_" + writeFileName[2] + "__" + previousFile[2] + ".csv"
writer = open(writeFileName, 'w')
writer = csv.DictWriter(writer, delimiter=",", fieldnames=headers, lineterminator='\n')
writer.writeheader()

print "Comparing started. Please wait."
#iteracija kroz retke trenutne verzije
for indexCurrentVersion, currentVersionRow in enumerate(readerCurrentFileDict):

	#iteracija kroz sve verzije s kojima se zeli usporedjivati
	for indexCurrentCompareVersion, currentCompareFile in enumerate(readerCompareList):

		#postavljanje trenutne usporedne datoteke na pocetak
		readerCompareListFiles[indexCurrentCompareVersion].seek(0)
		#za svaku iteraciju osim prve pomakni na drugi redak jer prvi sadrzi header
		if indexCurrentVersion != 0:
			readerCompareListFiles[indexCurrentCompareVersion].next()

		#postavljanje flag varijable na 0, varijabla prati da li klasa postoji u obje datoteke
		flag = 0
		for compareVersionRow in currentCompareFile:
			if currentVersionRow['File'] == compareVersionRow['File']: #ako klasa postoji u obje datoteke

				#postavljanje flaga na 1 i dodavanje true vrijednosti u stupac ClassFound
				flag = 1
				currentVersionRow['ClassFound - V' + str(selectedVersion[indexCurrentCompareVersion])] = '1'
				
				#racunanje defect density
				currentVersionRow['DD'] = float(currentVersionRow['bug_cnt']) / float(currentVersionRow['LOC'])
				#racunanje prosjecnog broja gresaka
				currentVersionRow['bug_cnt_avg'] = float(currentVersionRow['bug_cnt']) / float(bug_cnt_SUM)
				#racunanje prosjecnog broja linija koda (LOC)
				currentVersionRow['LOC_avg'] = float(currentVersionRow['LOC']) / float(LOCsum)
				#postavljanje vrijednosti klasifikatora na 0 ili 1 ovisno o broju LOC-a
				if (float(currentVersionRow['bug_cnt']) > 0.0 ):
					currentVersionRow['bug_cnt_bin'] = 1
				else:
					currentVersionRow['bug_cnt_bin'] = 0
					
				#iteracija kroz koju se racunaju apsolutna i relativna razlika
				#nakon izracuna dobivene vrijednosti zapisuju se u odgovarajuce stupce
				for i in range(1, headersLenMetric):
					#izracun apsolutne razlike
					absoluteDifference = abs(float(currentVersionRow[headersMetric[i]]) - float(compareVersionRow[headersMetric[i]]))
					#zapisivanje apsolutne razlike
					currentVersionRow['Absolute difference ' + headersMetric[i] + " - V" + str(selectedVersion[indexCurrentCompareVersion])] = str(absoluteDifference)
					#ako je vrijednost 0 formatiramo relativnu razliku na float 0 s dvije decimale
					if float(currentVersionRow[headersMetric[i]]) == 0.0:
						relativeDifference = format(0, '.2f')
					else:
						#izracun relativne razlike i formatiranje na float s dvije decimale
						relativeDifference = format(( absoluteDifference / float(currentVersionRow[headersMetric[i]]) ), '.2f')
					#zapisivanje relativne razlike za odgovarajucu metriku
					currentVersionRow['Relative difference ' + headersMetric[i] + " - V" + str(selectedVersion[indexCurrentCompareVersion])] = str(relativeDifference)
		#ako nismo pronasli iste klase u stupac ClassFound zapisujemo false vrijednost
		if flag == 0:
			currentVersionRow['ClassFound - V' + str(selectedVersion[indexCurrentCompareVersion])] = '0'
			#racunanje defect density
			currentVersionRow['DD'] = float(currentVersionRow['bug_cnt']) / float(currentVersionRow['LOC'])
			#racunanje prosjecnog broja gresaka
			currentVersionRow['bug_cnt_avg'] = float(currentVersionRow['bug_cnt']) / float(bug_cnt_SUM)
			#racunanje prosjecnog broja linija koda (LOC)
			currentVersionRow['LOC_avg'] = float(currentVersionRow['LOC']) / float(LOCsum)
			#postavljanje vrijednosti klasifikatora na 0 ili 1 ovisno o broju LOC-a
			if (float(currentVersionRow['bug_cnt']) > 0.0 ):
				currentVersionRow['bug_cnt_bin'] = 1
			else:
				currentVersionRow['bug_cnt_bin'] = 0
				
			#iteracija kroz koju se zapisuju false vrijednosti za sve metrike jer nismo pronasli navedenu klasu
			for i in range(1, headersLenMetric):
				currentVersionRow['Absolute difference ' + headersMetric[i] + " - V" + str(selectedVersion[indexCurrentCompareVersion])] = '0'
				currentVersionRow['Relative difference ' + headersMetric[i] + " - V" + str(selectedVersion[indexCurrentCompareVersion])] = '0'
	#zapisivanje retka u izlaznu datoteku
	writer.writerow(currentVersionRow)

#obavjestava se korisnik o uspjesnom izvodjenju programa
print "Comparing info: success"
