import os
import sys
import csv
import glob
import re
import pandas

#unos putanje i trenutne verzije, prazan input se ne prihvaca
flag = 1;
while flag:
	path = raw_input("Please enter path: ")

	if not path:
		print "Please enter path again!"
	else:
		#dohvat putanje direktorija koji sadrzi sve verzije programskog koda
		fmask = os.path.join(path, '*.csv')
		csvFiles = glob.glob(fmask)
		if not csvFiles:
			print "Please enter path again. There is no files under that path."
		else:
			flag = 0

#ispis svih csv datoteka uz indekse

print "Available versions:"
i = 0
files = []
for index, file in sorted(enumerate(csvFiles), key=lambda file: file[1]):
	print str(i) + " - " + file #dohvati sve csv fileove iz zadanog direktorija
	files.append(file)
	i=i+1

#od korisnika se trazi unos indeksa trenutne verzije
flag = 1;
while flag:
	currentFileIndex = raw_input("Please enter current version index: ")
	#provjerava se je li indeks trenutne verzije datoteke validan
	if currentFileIndex.isdigit() and int(currentFileIndex) < len(csvFiles):
		#postavlja se indeks trenutne verzije i sprema se trenutna verzija datoteke u currentFile
		currentFileIndex = int(currentFileIndex)
		currentFile = files[currentFileIndex]
		flag = 0;
	else:
		print "Current version index is not valid."

#zapis indexa svih starijih verzija datoteka u selectedVersion
selectedVersion = []
for index in range(currentFileIndex):
	selectedVersion.append(index)

#otvaranje trenutne verzije datoteke te zapisivanje rijecnika u readerCurrentFileDict varijablu
readerCurrentFile = open(currentFile, 'r')
readerCurrentFileDict = csv.DictReader(readerCurrentFile, delimiter=",", quoting=csv.QUOTE_NONE)

#izrada polja headers i ucitavanje imena klase te svih metrika trenutne verzije
headers = []
headersMetric = readerCurrentFileDict.fieldnames
headersLenMetric = len(headersMetric)

#import data by columns
data = pandas.read_csv(currentFile)
bug_cnt_SUM = 0
LOCsum = 0
#make sum for LOC and bug_cnt column
#print data.LOC
LOCsum = sum(data.LOC)
#print LOCsum
bug_cnt_SUM = sum(data.bug_cnt)

#prvo se u zaglavlje doda ime klase
headers.append(readerCurrentFileDict.fieldnames[0])
#iteracija kroz sve metrike
for field in readerCurrentFileDict.fieldnames:
	#preskace se File zato jer to nije metrika
	if field == "File":
		continue
	#u zaglavlje se dodaje ime metrike
	headers.append(field)
	#dodaju se apsolutna i relativna razlika za tu metriku
	headers.append('Absolute difference ' + field)
	headers.append('Relative difference ' + field)

#dodaje se stupac za defect density
headers.append('DD')
#dodaj stupac za izracun prosjecnog broja pogresaka
headers.append('bug_cnt_avg')
#dodaj stupac za izracun prosjecnog broja linija koda
headers.append('LOC_avg')
#dodaj stupac za binarni klasifikator
headers.append('bug_cnt_bin')
#dodaje se stupac koji prikazuje ako je klasa pronadjena i ako je u kojoj verziji je pronadjena
headers.append('ClassFound')
headers.append('Version')

#otvaranje svih verzija datoteka s kojima se zeli usporedjivati
readerCompareListFiles = []
readerCompareList = []
for v in selectedVersion:
	#otvaranje datoteke i pohrana u rijecnik
	readerFileCurrentVersion = open(csvFiles[v], 'r')
	readerFileCurrentVersionDict = csv.DictReader(readerFileCurrentVersion, delimiter=",", quoting=csv.QUOTE_NONE)
	#spremanje svih verzija u listu za usporedbu
	readerCompareList.append(readerFileCurrentVersionDict)
	readerCompareListFiles.append(readerFileCurrentVersion)

#otvaranje datoteke za zapisivanje rezultata mjerenja te zapisivanje headera
#writeFileName = re.findall(r'/(\w+)',currentFile)
#writeFileName = re.search('/', currentFile).groups()
writeFileName = re.split(r'(\W+)', currentFile)
writeFileName =  "writeOldestResults_" + writeFileName[2] + ".csv"
writer = open(writeFileName, 'w')
writer = csv.DictWriter(writer, delimiter=",", fieldnames=headers, lineterminator='\n')
writer.writeheader()

print "Comparing started. Please wait."
#stvaranje varijable koja ce sadrzavati sve pronadjene klase unutar trenutne verzije u nekoj od starijih verzija
foundClasses = []
#iteracija kroz retke trenutne verzije
for indexCurrentVersion, currentVersionRow in enumerate(readerCurrentFileDict):	
	#iteracija kroz sve verzije koje su starije od trenutne verzije
	for indexCurrentCompareVersion, currentCompareFile in enumerate(readerCompareList):
		#postavljanje trenutne usporedne datoteke na pocetak
		readerCompareListFiles[indexCurrentCompareVersion].seek(0)
		#za svaku iteraciju osim prve pomakni na drugi redak jer prvi sadrzi header
		if indexCurrentVersion != 0:
			readerCompareListFiles[indexCurrentCompareVersion].next()

		#postavljanje flag varijable na 0, varijabla prati da li klasa postoji u nekoj od starijih verzija
		flag = 0
		for compareVersionRow in currentCompareFile:
			#ako je klasa vec pronadjena u nekoj od prethodnih verzija postavlja se flag na true i preskace se izvodjenje petlje za tu klasu
			if currentVersionRow['File'] in foundClasses:
				flag = 1
				continue
			if currentVersionRow['File'] == compareVersionRow['File']: #ako klasa postoji u obje datoteke
				#racunanje defect density
				currentVersionRow['DD'] = float(currentVersionRow['bug_cnt']) / float(currentVersionRow['LOC'])
				#racunanje prosjecnog broja gresaka
				currentVersionRow['bug_cnt_avg'] = float(currentVersionRow['bug_cnt']) / float(bug_cnt_SUM)
				#racunanje prosjecnog broja linija koda (LOC)
				currentVersionRow['LOC_avg'] = float(currentVersionRow['LOC']) / float(LOCsum)
				#postavljanje vrijednosti klasifikatora na 0 ili 1 ovisno o broju LOC-a
				if (float(currentVersionRow['bug_cnt']) > 0.0 ):
					currentVersionRow['bug_cnt_bin'] = 1
				else:
					currentVersionRow['bug_cnt_bin'] = 0
				#postavljanje flaga na 1 i dodavanje true vrijednosti u stupac ClassFound
				flag = 1
				currentVersionRow['ClassFound'] = '1'
				currentVersionRow['Version'] = csvFiles[indexCurrentCompareVersion]
				#iteracija kroz koju se racunaju apsolutna i relativna razlika
				#nakon izracuna dobivene vrijednosti zapisuju se u odgovarajuce stupce
				for i in range(1, headersLenMetric):
					#izracun apsolutne razlike
					absoluteDifference = abs(float(currentVersionRow[headersMetric[i]]) - float(compareVersionRow[headersMetric[i]]))
					#zapisivanje apsultne razlike
					currentVersionRow['Absolute difference ' + headersMetric[i]] = str(absoluteDifference)
					#formatiranje relativne razlike na float vrijednost s dvije decimale ako je metrika 0
					if float(currentVersionRow[headersMetric[i]]) == 0.0:
						relativeDifference = format(0, '.2f')
					#racunanje relativne razlike te formatiranje relativne razlike na dvije decimale
					else:
						relativeDifference = format(( absoluteDifference / float(currentVersionRow[headersMetric[i]]) ), '.2f')#.replace('.', ',')
					#zapisivanje relativne razlike
					currentVersionRow['Relative difference ' + headersMetric[i]] = str(relativeDifference)
				#dodavanje klase u listu pronadjenih klasa
				foundClasses.append(currentVersionRow['File'])
		#ako nismo pronasli klasu u niti jednoj prethodnoj verziji
		if flag == 0:
			#racunanje defect density
			currentVersionRow['DD'] = float(currentVersionRow['bug_cnt']) / float(currentVersionRow['LOC'])
			#racunanje prosjecnog broja gresaka
			currentVersionRow['bug_cnt_avg'] = float(currentVersionRow['bug_cnt']) / float(bug_cnt_SUM)
			#racunanje prosjecnog broja linija koda (LOC)
			currentVersionRow['LOC_avg'] = float(currentVersionRow['LOC']) / float(LOCsum)
			#postavljanje vrijednosti klasifikatora na 0 ili 1 ovisno o broju LOC-a
			if (float(currentVersionRow['bug_cnt']) > 0.0 ):
				currentVersionRow['bug_cnt_bin'] = 1
			else:
				currentVersionRow['bug_cnt_bin'] = 0
			#postavljanje ClassFound na false
			currentVersionRow['ClassFound'] ='0'
			#Version se postavlja na none jer klasa nije pronadjena u niti jednoj prethodnoj verziji
			currentVersionRow['Version'] = 'none'
			#iteracija kroz koju se zapisuju false vrijednosti za sve metrike jer nismo pronasli navedenu klasu
			for i in range(1, headersLenMetric):
				currentVersionRow['Absolute difference ' + headersMetric[i]] = '0'
				currentVersionRow['Relative difference ' + headersMetric[i]] = '0'
	#zapisujemo redak u izlaznu datoteku
	writer.writerow(currentVersionRow)

#korisnika se obavjestava o uspjesnoj provedbi programa
print "Comparing info: success"

